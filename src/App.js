import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import Posts from './components/Posts';
import Post from './components/Post';

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/posts' component={Posts}/>
          <Route path='/posts/:postId' component={Post}/>
        </Switch>
      </div>
    )
  }
}

export default App;