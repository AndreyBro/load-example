import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
    return (
      <div className='posts'>
        <Link to={'/posts'}>
          <h1>Click for get posts</h1>
        </Link>
      </div>
    )
  }
}

export default Home;