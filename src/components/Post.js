import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPostAction } from '../store/actions/fetchPostAction';


class Post extends Component {
  componentDidMount() {
    const postId = this.props.match.params.postId;
    this.props.fetchPostAction(postId);
  }
  render() {
    const postId = this.props.match.params.postId;
    const prevData = this.props.posts.list[postId];

    const { post } = this.props.post;
    return (
      <div className='posts'>
        <Link to={'/posts'}>
          <h1>All Posts</h1>
        </Link>
        <div className='posts-list'>
          <div className='prevData'>
            <p>{(!prevData || !prevData.id) ? post.id : prevData.id}</p>
            <p>{(!prevData || !prevData.name) ? post.name : prevData.name}</p>
            <p>{(!prevData || !prevData.username) ? post.username : prevData.username}</p>
            <p>{(!prevData || !prevData.email) ? post.email : prevData.email}</p>
          </div>
          {
            (Object.keys(post).length === 0) ?
              <h1 className='spiner'>Loading...</h1>
              :
              <div className='fullData'>
                <p>{post.address.street}</p>
                <p>{post.address.suite}</p>
                <p>{post.address.city}</p>
                <p>{post.address.zipcode}</p>
                <p>{post.address.geo.lat}</p>
                <p>{post.address.geo.lng}</p>
                <p>{post.phone}</p>
                <p>{post.website}</p>
                <p>{post.company.name}</p>
                <p>{post.company.catchPhrase}</p>
                <p>{post.company.bs}</p>
              </div>
          }
        </div>
      </div>
    )
  }
}


const mapStateToProps = store => {
  return {
    posts: store.posts,
    post: store.post
  }
};

const mapDispatchToProps = dispatch => ({
  fetchPostAction: post => dispatch(fetchPostAction(post))
});

export default connect(mapStateToProps, mapDispatchToProps)(Post);