import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPostsAction } from '../store/actions/fetchPostsAction';
import PostsItem from './PostsItem';


class List extends Component {
  componentDidMount() {
    this.props.fetchPostsAction();
  }
  render() {
    const { list } = this.props;
    return (
      <div className='posts'>
        <Link to={'/'}>
          <h1>Home</h1>
        </Link>
        <PostsItem posts={list}/>
      </div>
    )
  }
}

const mapStateToProps = store => {
  return {
    list: store.posts.list
  }
};

const mapDispatchToProps = dispatch => ({
  fetchPostsAction: posts => dispatch(fetchPostsAction(posts))
});

export default connect(mapStateToProps, mapDispatchToProps)(List);