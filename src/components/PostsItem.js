import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Posts extends Component {
  render() {
    const { posts } = this.props;
    return (
      (posts && posts.length) ?
        <ul className='posts-list'>
          {
            posts.map(post => {
              return <li className='post' key={post.id}>
                <Link to={`/posts/${post.id - 1}`}>
                  <p>{post.name}</p>
                  <p>{post.username}</p>
                  <p>{post.email}</p>
                  <p>{post.website}</p>
                </Link>
              </li>
            })
          }
        </ul>
        :
        <h1>Loading...</h1>
    )
  }
}

export default Posts;