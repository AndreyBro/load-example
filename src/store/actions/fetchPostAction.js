import axios from 'axios';
export const FETCH_POST = 'FETCH_POST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAIL = 'FETCH_POST_FAIL';


export const fetchPostAction = (postId) => (dispatch) => {
  dispatch({
    type: FETCH_POST
  });

  axios.get('./../../../fullPosts.json')
    .then(
      res => {
        const post = res.data[postId];
        setTimeout(() => {
          dispatch({ type: 'FETCH_POST_SUCCESS', post });
        }, 1000);
      },
      error => {
        dispatch({ type: 'FETCH_POST_FAIL', error });
        throw error
      }
    )
};