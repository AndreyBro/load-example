import { 
  FETCH_POST,
  FETCH_POST_FAIL,
  FETCH_POST_SUCCESS,
} from '../actions/fetchPostAction';

const initialState = {
  post: {}
}

const postReducer = function (state = initialState, action) {  
  switch (action.type) {
  case FETCH_POST:    
    return {
      ...state,
      isLoadedPosts: false
    };
  case FETCH_POST_SUCCESS:  
    return {
      ...state,
      isLoadedPosts: true,
      post: action.post
    };
  case FETCH_POST_FAIL:
    return {
      ...state,
      isLoadedPosts: true
    };
  default:
    return state;
  }
};

export default postReducer;